data "vkcs_networking_network" "extnet" {
  name = "ext-net"
}

resource "vkcs_networking_network" "tgnet1" {
  name = "tgnet1"
}

resource "vkcs_networking_subnet" "tgsnet1" {
  name       = "tgsnet1"
  network_id = vkcs_networking_network.tgnet1.id
  cidr       = "192.168.199.0/24"
}

resource "vkcs_networking_router" "router_2290" {
  name                = "router_2290"
  admin_state_up      = true
  external_network_id = data.vkcs_networking_network.extnet.id
}

resource "vkcs_networking_router_interface" "compute" {
  router_id = vkcs_networking_router.router_2290.id
  subnet_id = vkcs_networking_subnet.tgsnet1.id
}

resource "vkcs_networking_floatingip" "fip" {
  count=var.instances_count
  pool = data.vkcs_networking_network.extnet.name
}

resource "vkcs_compute_floatingip_associate" "fip" {
  count       = var.instances_count
  floating_ip = vkcs_networking_floatingip.fip[count.index].address
  instance_id = vkcs_compute_instance.instance[count.index].id
}

# resource "vkcs_networking_floatingip" "fip-app" {
#   pool = data.vkcs_networking_network.extnet.name
# }

# resource "vkcs_compute_floatingip_associate" "fip-app" {
#   floating_ip = vkcs_networking_floatingip.fip-app.address
#   instance_id = vkcs_compute_instance.app.id
# }


# data "vkcs_networking_network" "tgnet1" {
#   name = "tgnet1"
# }


# data "vkcs_networking_subnet" "tgsnet1" {
#   name       = "tgsnet1"
# }


# data "vkcs_networking_router" "router_2290" {
#   name       = "router_2290"
# }


# resource "vkcs_networking_floatingip" "fip-app" {
#   pool = data.vkcs_networking_network.extnet.name
# }

# resource "vkcs_compute_floatingip_associate" "fip-app" {
#   floating_ip = vkcs_networking_floatingip.fip-app.address
#   instance_id = vkcs_compute_instance.instance.0.id
#}

# resource "vkcs_networking_floatingip" "fip" {
#   count=var.instances_count
#   pool = data.vkcs_networking_network.extnet.name 
# }

# resource "vkcs_compute_floatingip_associate" "fip" {
#   count       = var.instances_count
#   floating_ip = vkcs_networking_floatingip.fip[count.index].address
#   instance_id = vkcs_compute_instance.instance[count.index].id
# }